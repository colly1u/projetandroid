package com.iut.guner_colly.projettodolist;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class AddItemActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addItem(View view) {
        EditText itemForm = ((EditText) findViewById(R.id.itemLabel));
        Item item = new Item(itemForm.getText().toString());
        item.save(this);
        setResult(RESULT_OK);
        finish();
    }

    public void DelItem(View view) {
        EditText itemForm = ((EditText) findViewById(R.id.itemLabel));
        Item item = new Item(itemForm.getText().toString());
        item.deleteItem(this);
        setResult(RESULT_OK);
        finish();


    }
}
